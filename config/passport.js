const LocalStrategy = require("passport-local").Strategy;
const db = require("../models");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const bcrypt = require("bcrypt");

function configureLocalStrategy() {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "emailAddress",
        session: false
      },
      function(username, password, done) {
        db.User.findOne({ emailAddress: username }, function(err, user) {
          if (err) {
            return done(err);
          }

          // if user cannot be found then return  null user
          if (!user) {
            return done(null, false);
          }

          user.comparePassword(password).then(match => {
            // if password doesn't match then return null user
            if (!match) {
              return done(null, false);
            }

            const {
              id,
              emailAddress,
              firstName,
              lastName,
              profileImageUrl
            } = user;

            const payload = {
              sub: id
            };

            // create the token and then return to callback
            let token = jwt.sign(payload, process.env.JWT_KEY);
            return done(null, user, {
              token,
              emailAddress,
              firstName,
              lastName,
              profileImageUrl
            });
          });
        });
      }
    )
  );
}

function configureJwtStrategy() {
  const opts = {};

  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = process.env.JWT_KEY;

  passport.use(
    new JwtStrategy(opts, function(jwtPayload, done) {
      console.log("jwt strategy");
      db.User.findById({ _id: jwtPayload.sub }, function(err, user) {
        if (err) {
          console.log("api not authenticate");
          return done(err, false);
        }

        if (user) {
          return done(null, user);
        } else {
          return done(null, false);
        }
      });
    })
  );
}

module.exports = { configureLocalStrategy, configureJwtStrategy };
