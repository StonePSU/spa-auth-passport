const db = require("../models");

const userHandler = {
  getAllUsers: async function(req, res, next) {
    try {
      let users = await db.User.find().select(
        "id emailAddress firstName lastName profileImageUrl createdAt updatedAt"
      );
      res.status(200).json(users);
    } catch (err) {
      return next(err);
    }
  },

  getUserById: async function(req, res, next) {
    let userId = req.params.id;
    try {
      let user = await db.User.findById(userId);
      res.status(200).json(user);
    } catch (err) {
      return next(err);
    }
  },

  deleteUserById: async function(req, res, next) {
    let userId = req.params.id;
    try {
      await db.User.remove({ _id: userId });
      res.status(200).json("User deleted successfully");
    } catch (err) {
      return next(err);
    }
  }
};

module.exports = userHandler;
