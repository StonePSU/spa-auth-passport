const passport = require("passport");
const jwt = require("jsonwebtoken");
const db = require("../models");
const bcrypt = require("bcrypt");

function signin(req, res, next) {
  return passport.authenticate(
    "local",
    { session: false },
    (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return next(err);
      }

      req.login(user, function(err) {
        return res.status(200).json(info);
      });
    }
  )(req, res, next);
}

async function signup(req, res, next) {
  try {
    let user = await db.User.create(req.body);
    let { emailAddress, firstName, lastName, profileImageUrl, id } = user;

    token = jwt.sign({ sub: user.id }, process.env.JWT_KEY);
    res.status(200).json({
      token,
      emailAddress,
      firstName,
      lastName,
      profileImageUrl
    });
  } catch (err) {
    if (err.code === 11000) {
      err.message = "Username already exists";
    }
    next(err);
  }
}

module.exports = { signin, signup };
