const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv").config();
const morgan = require("morgan");
const passport = require("passport");
require("./config/passport.js").configureJwtStrategy();
require("./config/passport.js").configureLocalStrategy();

const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/users");

mongoose.set("debug", false);
mongoose.Promise = Promise;
mongoose.connect(
  process.env.MONGODB_URI,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    keepAlive: true
  }
);

app.use(passport.initialize());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(morgan("combined"));

app.get("/", function(req, res, next) {
  throw new Error("shit!!!");
});

app.use("/auth", authRoutes);
app.use("/api/user", userRoutes);
/*app.get("/api/user", jwtMw(passport), function(req, res, next) {
  res.json(req.headers.authorization);
});*/

// if no other routes match then we will get into this middle ware
// create a new error and then call next(error) to pass to the error handler
app.use(function(req, res, next) {
  let error = new Error("Page Not Found");
  error.status = 401;
  next(error);
});

// generic error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).send(err.message);
});

app.listen(8080, () => {
  console.log("THE SERVER IS RUNNING");
});
