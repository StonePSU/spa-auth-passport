const router = require("express").Router();
const userHandler = require("../handlers/users");
const authMiddleware = require("../middleware/auth");

router.use(authMiddleware());
router.route("/").get(userHandler.getAllUsers);

module.exports = router;
