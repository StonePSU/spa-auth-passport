const router = require("express").Router();
const auth = require("../handlers/auth");

router.route("/signin").post(auth.signin);
router.route("/signup").post(auth.signup);

module.exports = router;
